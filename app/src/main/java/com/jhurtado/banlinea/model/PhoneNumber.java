package com.jhurtado.banlinea.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by juan hurtado on 15/12/17.
 */

public class PhoneNumber {
    /**
     * Phone number Model that matches the web service model
     */
    @SerializedName("Country")
    Country country;

    @SerializedName("Number")
    String number;

    public void setCountry(Country country){
        this.country = country;
    }

    public void setNumber(String number){
        this.number = number;
    }
}
