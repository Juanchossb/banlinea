package com.jhurtado.banlinea.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by juan hurtado on 15/12/17.
 */

public class Location {
    /**
     * Location Model that matches the web service model
     */
    @SerializedName("Latitude")
    private double latitude;

    @SerializedName("Longitude")
    private double longitude;

    public void setLocation(double latitude, double longitude){
        this.latitude = latitude;
        this.longitude = longitude;
    }
}
