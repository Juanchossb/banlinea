package com.jhurtado.banlinea.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by juan hurtado on 15/12/17.
 */

public class Country {
    /**
     * Country Model that matches the web service model
     */
    @SerializedName("Code")
    int code;

    @SerializedName("Name")
    String name;

    @Override
    public String toString() {
        return name;
    }

    public String getName(){
        return name;
    }
}
