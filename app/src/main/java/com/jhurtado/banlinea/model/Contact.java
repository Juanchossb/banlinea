package com.jhurtado.banlinea.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by juan hurtado on 15/12/17.
 */

public class Contact {
    /**
     * Contact Model that matches the web service model
     */

    @SerializedName("Company")
    String company;

    @SerializedName("EmailsAddress")
    List<String> emails;

    @SerializedName("LastName")
    String lastName;

    @SerializedName("Name")
    String name;

    @SerializedName("PhoneNumbers")
    List<PhoneNumber> phoneNumbers;

    @SerializedName("Photo")
    String photo;

    public void setCompany(String company){
        this.company = company;
    }

    public void setEmails(List<String> list){
        this.emails = list;
    }

    public void setLastName(String lastName){
        this.lastName = lastName;
    }

    public void setFirstName(String firstName){
        this.name = firstName;
    }

    public void setPhoneNumbers(List<PhoneNumber> list){
        this.phoneNumbers = list;
    }

    public void setPhoto(String base64){
        this.photo = base64;
    }

}
