package com.jhurtado.banlinea.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by juan hurtado on 15/12/17.
 */

public class Aspirant {
    /**
     * Aspirant Model that matches the web service model
     */
    @SerializedName("Name")
    String Name="Juan Manuel Hurtado Romero";
}
