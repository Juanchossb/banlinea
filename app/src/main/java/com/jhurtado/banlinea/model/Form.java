package com.jhurtado.banlinea.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by juan hurtado on 15/12/17.
 */

public class Form {
    /**
     * Form Model that matches the web service model
     */
    @SerializedName("Contacts")
    List<Contact> contactList;
    @SerializedName("Location")
    Location location;
    @SerializedName("RegisteredBy")
    Aspirant aspirant;
    @SerializedName("Type")
    int type = 1;

    public void setContactList(List<Contact> contactList){
        this.contactList = contactList;
    }

    public void setLocation(Location location){
        this.location = location;
    }

}
