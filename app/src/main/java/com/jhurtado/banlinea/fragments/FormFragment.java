package com.jhurtado.banlinea.fragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.jhurtado.banlinea.R;
import com.jhurtado.banlinea.model.Contact;
import com.jhurtado.banlinea.model.Country;
import com.jhurtado.banlinea.model.Form;
import com.jhurtado.banlinea.model.PhoneNumber;
import com.jhurtado.banlinea.networking.NetworkManager;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.app.Activity.RESULT_OK;

/**
 * Created by juan hurtado on 15/12/17.
 */

public class FormFragment extends Fragment {

    private EditText editCompany;
    private EditText editEmail;
    private EditText editLastName;
    private EditText editFirstName;
    private EditText editPhoneNumber;
    private Button buttonPhoto;
    private Button buttonSave;
    private Button buttonUpload;
    private Spinner spinnerCountry;
    private ProgressBar loading;
    private static final int FORM_FRAGMENT_TAG = 1;

    private String base64Image;
    private NetworkManager networkManager;
    private List<Country> countryList;
    private ImageView imageView;
    Location posicion;
    private List<Contact> savedContactList = new ArrayList<>();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_form, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        editCompany = (EditText) view.findViewById(R.id.edit_company);
        editEmail = (EditText) view.findViewById(R.id.edit_email);
        editLastName = (EditText) view.findViewById(R.id.edit_last_name);
        editFirstName = (EditText) view.findViewById(R.id.edit_first_name);
        editPhoneNumber = (EditText) view.findViewById(R.id.edit_phone);
        buttonPhoto = (Button) view.findViewById(R.id.button_photo);
        buttonSave = (Button) view.findViewById(R.id.button_save);
        buttonUpload = (Button) view.findViewById(R.id.button_upload);
        spinnerCountry = (Spinner) view.findViewById(R.id.spinner_country);
        imageView = (ImageView) view.findViewById(R.id.view_image);
        loading = (ProgressBar) view.findViewById(R.id.loading);
        buttonUpload.setText(String.format(getString(R.string.upload_info_prompt), savedContactList.size()));

        networkManager = new NetworkManager();
        networkManager.getCountryList().enqueue(countryListCallback);

        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                posicion = location;
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        });

        buttonPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, FORM_FRAGMENT_TAG);
                }
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Contact contact = getInformation();
                if (contact != null) {
                    savedContactList.add(contact);
                    editCompany.setText("");
                    editFirstName.setText("");
                    editLastName.setText("");
                    editPhoneNumber.setText("");
                    editEmail.setText("");
                    imageView.setVisibility(View.GONE);
                    buttonUpload.setText(String.format(getString(R.string.upload_info_prompt), savedContactList.size()));
                    buttonUpload.setEnabled(true);
                }
            }
        });

        buttonUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                com.jhurtado.banlinea.model.Location location = new com.jhurtado.banlinea.model.Location();
                if (posicion != null)
                    location.setLocation(posicion.getLatitude(), posicion.getLongitude());
                final Form form = new Form();
                form.setContactList(savedContactList);
                form.setLocation(location);
                loading.setVisibility(View.VISIBLE);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        networkManager.uploadContacts(form).enqueue(uploadCallback);
                    }
                }).start();
            }
        });
    }

    private Contact getInformation() {
        Contact contact = new Contact();
        if (editCompany.getText().toString().length() == 0) {
            editCompany.setError(getString(R.string.error_empty_field));
        } else if (editEmail.getText().toString().length() == 0) {
            editEmail.setError(getString(R.string.error_empty_field));
        } else if (!editEmail.getText().toString().contains("@") || !editEmail.getText().toString().contains(".")) {
            editEmail.setError(getString(R.string.error_invalid_email));
        } else if (editLastName.getText().toString().length() == 0) {
            editLastName.setError(getString(R.string.error_empty_field));
        } else if (editFirstName.getText().toString().length() == 0) {
            editFirstName.setError(getString(R.string.error_empty_field));
        } else if (editPhoneNumber.getText().toString().length() == 0) {
            editPhoneNumber.setError(getString(R.string.error_empty_field));
        } else {
            contact.setCompany(editCompany.getText().toString());
            contact.setFirstName(editFirstName.getText().toString());
            contact.setLastName(editLastName.getText().toString());
            contact.setPhoto(base64Image);
            List<String> emailList = new ArrayList<>();
            emailList.add(editEmail.getText().toString());
            contact.setEmails(emailList);
            List<PhoneNumber> phoneList = new ArrayList<>();
            PhoneNumber phonenumber = new PhoneNumber();
            phonenumber.setNumber(editPhoneNumber.getText().toString());
            phonenumber.setCountry(countryList.get(spinnerCountry.getSelectedItemPosition()));

            return contact;
        }
        return null;
    }

    private Callback<Integer> uploadCallback = new Callback<Integer>() {
        @Override
        public void onResponse(Call<Integer> call, Response<Integer> response) {
            loading.setVisibility(View.GONE);
            int code = response.body();
            if (code == 200) {
                Toast.makeText(getContext(), getString(R.string.upload_succesful), Toast.LENGTH_LONG).show();
                savedContactList.clear();
                buttonUpload.setText(String.format(getString(R.string.upload_info_prompt), savedContactList.size()));
                buttonUpload.setEnabled(false);
            } else {
                Toast.makeText(getContext(), getString(R.string.upload_failed), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFailure(Call<Integer> call, Throwable t) {

        }
    };

    private Callback<List<Country>> countryListCallback = new Callback<List<Country>>() {
        @Override
        public void onResponse(Call<List<Country>> call, Response<List<Country>> response) {
            countryList = response.body();
            ArrayAdapter<Country> adapter = new ArrayAdapter<Country>(getActivity(), android.R.layout.simple_spinner_dropdown_item, countryList);
            spinnerCountry.setAdapter(adapter);
        }

        @Override
        public void onFailure(Call<List<Country>> call, Throwable t) {

        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == FORM_FRAGMENT_TAG && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            imageView.setImageBitmap(imageBitmap);
            imageView.setVisibility(View.VISIBLE);

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            final byte[] byteArray = byteArrayOutputStream .toByteArray();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    base64Image = Base64.encodeToString(byteArray, Base64.NO_WRAP);
                }
            }).run();
        }
    }
}
