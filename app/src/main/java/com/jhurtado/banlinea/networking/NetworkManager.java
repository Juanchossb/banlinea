package com.jhurtado.banlinea.networking;

import com.jhurtado.banlinea.model.Country;
import com.jhurtado.banlinea.model.Form;

import org.json.JSONArray;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by juan hurtado on 15/12/17.
 */

public class NetworkManager {

    /**
     * Manager to handle all internet connections
     */

    private static String BASE_URL="https://contactmanager.banlinea.com/api/";
    private NetworkApi networkApi;

    public NetworkManager(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        networkApi = retrofit.create(NetworkApi.class);
    }

    public Call<List<Country>> getCountryList(){
        return networkApi.getCountryList();
    }

    public Call<Integer> uploadContacts(Form form){
        return networkApi.uploadContacts(form);
    }
}
