package com.jhurtado.banlinea.networking;

import com.jhurtado.banlinea.model.Country;
import com.jhurtado.banlinea.model.Form;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by juan hurtado on 15/12/17.
 */

public interface NetworkApi {
    /**
     * Retrofit interface to send GET and POST request to the server
     */
    @GET("countries")
    Call<List<Country>> getCountryList();

    @POST("ContactRegister")
    Call<Integer> uploadContacts(@Body Form form);
}
